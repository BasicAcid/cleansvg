# The MIT License (MIT)

# Copyright (c) 2018 David Tabarie <david.tabarie@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#! /usr/bin/env python3
__author__ = 'David Tabarie <david.tabarie@gmail.com>'
__version__ = 0.1
__license__ = "The MIT License (MIT)"

# A really trivial script to clean svg files from unecessary content,
# it will remove every line that contain words listed inside the 'catch' variable;
# I encourage you to make a copy of your files before running it.

# The following dependency is required, you need to place it inside the script directory:
# https://github.com/mahmoudadel2/pysed/blob/master/pysed.py

import sys
import argparse
from os import listdir
from os.path import isfile, join
from pysed import rmlinematch

parser = argparse.ArgumentParser()
parser.add_argument("cible", type=str, help="file or directory to clean")
parser.add_argument("-d", "--directory", help="Clean an entiere directory", action="store_true")
parser.add_argument("-s", "--skip", help="skip files without the .svg extension", action="store_true")
parser.add_argument("-c", "--c", help="make a copy of the files before cleaning", action="store_true")
parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
args = parser.parse_args()

catch = [
    "<title",
    "</title>",
    "<desc",
    "</desc>",
    "<defs",
    "</defs>",
    "<!--",
    "-->",
    "<g",
    "</g>",
    "<mask",
    "</mask>",
    "<use",
    "</use>",
    "<rect",
    "</rect>",
]

def f_verif(fn):
    """Check if it's an xml or svg file"""
    if not (fn[-4:] == ".xml" or fn[-4:] == ".svg"):
        return "Not xml or svg"
        
def clear_file(svgFile):
    for token in catch:
        if f_verif(svgFile) == "Not xml or svg":
            print(svgFile + " is not an xml or svg file")
            break
        rmlinematch(token, svgFile, dryrun=False)

def clear_dir(mypath):
    fileList = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    for f in fileList:
        path = mypath + f        
        if f_verif(path) == "Not xml or svg":
            print(path + " is not an xml or svg file")
            continue
        clear_file(path)
        
def main():
    if args.directory:
        clear_dir(args.cible)
    else:
        clear_file(args.cible)

main()
